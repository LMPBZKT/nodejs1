const { guardarDB, leerDB } = require('../helpers/guardarArchivo');
const Persona = require('../models/persona');
const Personas = require('../models/personas');

const { response } = 'express';

// instanciamos Personas, lo hacemos global para que se añada un nuevo usuario con cada petición POST
const personas = new Personas();

// Método para obtener una persona
const personaGet = (req, res = response) => {

  const personaDB = leerDB();

  if (personaDB) {
    personas.cargarPersonaFromArray(personaDB);
  }


  res.json({
    msg: 'get API - Controlador',
    // listado

    // Mostramos aqui la lista de personas
    personaDB
  })


};


// Método para actulizar una persona
const personaPut = (req, res = response) => {

  const { id } = req.params

  if (id) {
    personas.eliminarPersona(id)
  
    const { nombres, apellidos, ci, direccion, sexo } = req.body;
    const persona = new Persona(nombres, apellidos, ci , direccion, sexo);
  
    persona.setID(id)
  
    personas.crearPersona(persona);
    guardarDB(personas.listArr);
  }

  res.json({
    msg: 'Persona editada con exito'
  })
}


//añadir una persona 
const personaPost = (req, res = response) => {
  const { nombres, apellidos, ci, direccion, sexo } = req.body;

  const persona = new Persona(nombres, apellidos, ci , direccion, sexo);

  let personaDB = leerDB()

  if (personaDB) {
    personas.cargarPersonaFromArray(personaDB);
  }
 
  personas.crearPersona(persona);
  guardarDB(personas.listArr);
  personaDB = leerDB()

  const listado = leerDB();

  if (listado) {
    personas.cargarPersonaFromArray(listado);
  }

  res.json({
    msg: 'Persona añadida con exito',
    listado
  })

}


const personaDelete = (req, res = response) => {

  const { id } = req.params
  
  if (id) {
    personas.eliminarPersona(id);
    console.log("delete:",personas.listArr);
    guardarDB(personas.listArr)
    
  }


  res.json({
    msg: 'Persona eliminada con exito',
  })
};


module.exports = {
  personaGet,
  personaPut,
  personaPost,
  personaDelete
}